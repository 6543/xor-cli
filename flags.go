package main

import (
	"github.com/urfave/cli/v2"
)

var flags = []cli.Flag{
	&cli.StringSliceFlag{
		Required: true,
		Name:     "files",
		Usage:    "files to xor together, first file determine length of output",
	},
	&cli.BoolFlag{
		Name:  "reuse",
		Usage: "if true and a file is less than output lenght, use it multible times, else fail",
		Value: false,
	},
	&cli.StringFlag{
		Name:  "output",
		Value: "-",
		Usage: "use '-' to print to stdout",
	},
}
