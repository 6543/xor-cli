package main

import (
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {
	app := cli.NewApp()
	app.Name = "xor"
	app.Usage = "xor files with and without padding"
	app.Action = run
	app.Flags = flags

	if err := app.Run(os.Args); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func run(ctx *cli.Context) error {
	files := ctx.StringSlice("files")
	reuse := ctx.Bool("reuse")
	output := ctx.String("output")

	if len(files) < 2 {
		return fmt.Errorf("need at least two files")
	}

	reader := make([]io.ReadSeekCloser, len(files))
	var err error
	for i := range files {
		if files[i] == "-" {
			reader[i] = os.Stdin
		} else {
			reader[i], err = os.Open(files[i])
			if err != nil {
				return err
			}
			closer := reader[i]
			defer closer.Close()
		}
	}

	var writer io.WriteCloser
	if output == "-" {
		writer = os.Stdout
	} else {
		writer, err = os.Create(output)
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	bufferSize := 1024 * 2

	b1 := make([]byte, bufferSize)
	b2 := make([]byte, bufferSize)
	for {
		n, err := reader[0].Read(b1)
		if err != nil && !errors.Is(err, io.EOF) {
			return err
		}
		if n == 0 {
			return nil
		}

		for i := 1; i < len(reader); i++ {
			m, err := reader[i].Read(b2)
			if err != nil && !errors.Is(err, io.EOF) {
				return err
			}

			if m == 0 {
				return fmt.Errorf("file %s has no bytes to read from", files[i])
			}

			if m < n {
				if !reuse {
					return fmt.Errorf("file %s is shorter than output length", files[i])
				}
				reader[i].Seek(0, io.SeekStart)
				b3 := make([]byte, bufferSize-m)
				m, err = reader[i].Read(b3)
				if err != nil || m == 0 {
					return fmt.Errorf("could not read from %s %v", files[i], err)
				}
				b2 = append(b2, b3...)
			}

			for b := range b1 {
				b1[b] = b1[b] ^ b2[b]
			}
		}

		if _, err := writer.Write(b1); err != nil {
			return err
		}
	}
}
